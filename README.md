#Aircraft Admin Template
这个是一个基于Bootstrap v3.1.1的后台模板


#改进内容
2015-01-17
1、修改字体源为国内的360字体源，加载速度更快。
2、修改两处错误提示


#功能演示
http://aircraft.oschina.mopaas.com/index.html


#官方网站
http://portnine.com/bootstrap-themes/aircraft


#使用协议
可以免费使用,其他请参照Bootstrap协议
Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)